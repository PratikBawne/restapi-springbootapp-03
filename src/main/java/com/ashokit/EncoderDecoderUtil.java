package com.ashokit;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

public class EncoderDecoderUtil {

	public String encode(String text) {
		Encoder encoder = Base64.getEncoder();
		byte[] encode = encoder.encode(text.getBytes());
		return new String(encode);
	}
	
	public String decode(String encodedText) {
		Decoder decoder = Base64.getDecoder();
		byte[] decode = decoder.decode(encodedText);
		return new String(decode);
	}
	
}
